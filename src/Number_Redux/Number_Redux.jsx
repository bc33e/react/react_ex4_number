import React, { Component } from "react";
import { connect } from "react-redux";
import { increaseAction, decreaseAction } from "./redux/actions/numberAction";

class Number_Redux extends Component {
  render() {
    return (
      <div className="container py-5">
        <h1>Number Redux</h1>
        <h2 className="display-3">{this.props.soLuong}</h2>
        <button
          className="btn btn-info"
          onClick={() => {
            this.props.increase(1);
          }}
        >
          +
        </button>
        <button
          className="btn btn-danger"
          onClick={() => {
            this.props.decrease(1);
          }}
        >
          -
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    soLuong: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    increase: (soLuong) => {
      dispatch(increaseAction(soLuong));
    },
    decrease: (soLuong) => {
      dispatch(decreaseAction(soLuong));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Number_Redux);
