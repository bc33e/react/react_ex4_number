import { INCREASE, DECREASE } from "../constants/numberConstants";

const initialState = {
  number: 1,
};

export const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREASE: {
      state.number += action.payload;
      return { ...state };
    }
    case DECREASE: {
      state.number -= action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
