import { INCREASE, DECREASE } from "../constants/numberConstants";

export const increaseAction = (soLuong) => {
  return {
    type: INCREASE,
    payload: soLuong,
  };
};
export const decreaseAction = (soLuong) => {
  return {
    type: DECREASE,
    payload: soLuong,
  };
};
