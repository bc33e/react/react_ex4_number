import logo from "./logo.svg";
import "./App.css";
import Number_Redux from "./Number_Redux/Number_Redux";

function App() {
  return (
    <div className="App">
      <Number_Redux />
    </div>
  );
}

export default App;
